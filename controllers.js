var app = angular.module('myApp', ["ngRoute", 'ui.router', 'ngCookies', '720kb.datepicker', 'ui.bootstrap']);

app.controller('adminDashboard', function ($scope, $state) {
    $scope.adminLogin = function () {
        if ($scope.adminUsername === 'admin@admin.com' && $scope.adminPassword === 'pass') {
            console.log("success");
            $state.go('adminDash')
        } else console.log("error")
    }
});
app.config(function ($routeProvider, $urlRouterProvider, $stateProvider, $qProvider) {
    $qProvider.errorOnUnhandledRejections(false);

    $stateProvider
        .state('steps', {
            url: '/stepbystep/:fd/:fdebitor/:fcreditor/:fsum/:fstare',
            templateUrl: 'templates/steps.html',
            params: {
                fd: null,
                fdebitor: null,
                fcreditor: null,
                fsum: null
            }

        })
        .state('adminLogin', {
            url: "/admin",
            templateUrl: 'admin/mainPage.html'
        })
        .state('generateDoc', {
            url: "/generateDocs",
            templateUrl: 'templates/dosarDetailsSteps/stepsDetails/generateDocument.html'
        })

        .state('adminDash', {
            url: "/rootDash",
            templateUrl: 'admin/adminDashboard.html'
        })

        .state('login', {
            url: "/login",
            templateUrl: 'templates/login.html'
        })
        .state('viewsstepsdosar', {
            url: "/dosarDetailsMain/:fd/:fcreditor/:fdebitor",
            templateUrl: 'templates/dosarDetailsSteps/detaliiDosar.html',
            params: {
                fd: null
            }
        })
        .state('calendar', {
            url: "/calendar",
            templateUrl: 'templates/calendar.html'
        })
        .state('settings', {
            url: "/settings",
            templateUrl: 'templates/profile/profileSettings.html'
        })
        .state('dosareStat', {
            url: "/dosarestat",
            templateUrl: 'templates/dosareStat.html'
        })
        .state('dosarePensiiAlimentare', {
            url: "/dosarePensiiAlimentare",
            templateUrl: 'templates/dosarePensiiAlimentare.html'
        })
        .state('dosareCivile', {
            url: "/dosareCivile",
            templateUrl: 'templates/dosareCivile.html'
        })
        .state('dosareMele', {
            url: "/dosareMele",
            templateUrl: 'templates/dosareMele.html'
        })
        .state('registruIntari', {
            url: "/registruIntari",
            templateUrl: 'templates/registruIntari.html'
        })
        .state('registruIesirii', {
            url: "/registruIesirii",
            templateUrl: 'templates/registruIesirii.html'
        })
        .state('registruServicii', {
            url: "/registruServicii",
            templateUrl: 'templates/dosarePensiiAlimentare.html'
        })
        .state('rapoarte', {
            url: "/rapoarte",
            templateUrl: 'templates/rapoarte.html'
        })
        .state('dash', {
            url: "/dash",
            templateUrl: 'templates/dashboard.html'
        });
    $urlRouterProvider.otherwise('login');
});


app.controller("loginCtrl", function ($scope, $state, $rootScope, $cookies, $http) {
    $scope.login = function () {
        $rootScope.mail = $scope.username;
        $rootScope.password = $scope.password;

        var request = $http({
            method: 'post',
            url: 'http://localhost:3334/checkcredentials',
            data: ({
                login: $rootScope.mail,
                pass: $rootScope.password
            }),
            headers: {
                'Content-Type': 'application/json',
                'Accept': "application/json"
            }


        }).then(function successCallback(response, data) {
            console.log(response);
            $state.go('dash')
        }, function errorCallback(response) {
            $state.go('login')
        })
    }
});
app.controller('dosarePensiiAlimentare', function ($scope, $state, $http) {
    var request = $http({
        method: 'get',
        url: 'http://localhost:3334/dosarestat'
    }).then(function successCallback(response, data) {
        $scope.items = response.data;
        console.log(response);
    }, function errorCallback(response) {

    })
});

app.controller("dataCtrl", function ($scope, $state, $http) {
    var request = $http({
        method: 'post',
        url: 'http://localhost:3334/datactrlfornav'
    }).then(function successCallback(response, data) {
        $scope.items = response.data;
        console.log(response);
        $scope.settings = function () {
            $state.go('settings')
        };

    }, function errorCallback(response) {
        console.log(response)
    });
});

app.controller('namesCtrl', function ($scope, $http) {

    var request = $http({
        method: 'get',
        url: 'http://localhost:3334/dosarestat'
    }).then(function successCallback(response, data) {
        $scope.names = response.data;
        console.log(response.data);
    }, function errorCallback(response) {
    });
    $scope.orderByMe = function (x) {
        $scope.myOrderBy = x;
    }
});

app.controller('ExampleController', function ($scope, $http, $state, $stateParams) {
    var request = $http({
        method: 'get',
        url: 'http://localhost:3334/dosarestat'
    }).then(function successCallback(response, data) {
        $scope.names = response.data;
        console.log(response.data);
    }, function errorCallback(response) {
    });
    var friends = $scope.names;

    $scope.propertyName = 'age';
    $scope.reverse = true;
    $scope.friends = friends;

    $scope.sortBy = function (propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

});
app.controller("stepsCtrl", function ($scope, $state, $stateParams) {
    console.log($stateParams, "132123123");
    $scope.fd = $stateParams.fd;
    $scope.fdebitor = $stateParams.fdebitor;
    $scope.fcreditor = $stateParams.fcreditor;
    $scope.fsum = $stateParams.fsum;
    $scope.fstare = $stateParams.fstare;

    console.log($scope.fd)

});

app.controller("stepsCtrlDosare", function ($scope, $state, $stateParams) {
    console.log($stateParams, "132123123");
    $scope.fd = $stateParams.fd;
    $scope.fdebitor = $stateParams.fdebitor;
    $scope.fcreditor = $stateParams.fcreditor;
    $scope.fsum = $stateParams.fsum;
    $scope.fstare = $stateParams.fstare;
    console.log($scope.fd)

});

app.controller("WizardController", [wizardController]);

function wizardController() {
    var vm = this;

    vm.currentStep = 1;
    vm.steps = [
        {
            step: 1,
            name: "Document Executoriu",
            template: "templates/steps/step1.html"
        },
        {
            step: 2,
            name: "Debitor",
            template: "templates/steps/step2.html"
        },
        {
            step: 3,
            name: "Creditor",
            template: "templates/steps/step3.html"
        },
        {
            step: 4,
            name: "Copii",
            template: "templates/steps/step4.html"
        },
        {
            step: 5,
            name: "Achitari",
            template: "templates/steps/step5.html"
        },
        {
            step: 6,
            name: "Continut si Intentare",
            template: "templates/steps/step6.html"
        }
    ];
    vm.user = {};

    vm.gotoStep = function (newStep) {
        vm.currentStep = newStep;
    };

    vm.getStepTemplate = function () {
        for (var i = 0; i < vm.steps.length; i++) {
            if (vm.currentStep === vm.steps[i].step) {
                return vm.steps[i].template;
            }
        }
    };

    vm.save = function () {
        alert(
            "Saving form... \n\n" +
            "Name: " + vm.user.inst + "\n" +
            "Email: " + vm.user.document + "\n" +
            "Age: " + vm.user.age + "\n" +
            "Date" + vm.user.date);
    }
}

app.controller('statCtrl', function ($scope, $filter, filteredListService, $http) {
    var request = $http({
        method: 'get',
        url: 'http://localhost:3334/dosarestat'
    }).then(function successCallback(response, data) {
        $scope.names = response.data;
        console.log(response.data);
        $scope.pageSize = 10;
        $scope.sortReverse = false;
        $scope.searchSum = '';

        $scope.allItems = $scope.names;
        $scope.reverse = false;

        $scope.resetAll = function () {
            $scope.filteredList = $scope.allItems;
            $scope.newEmpId = '';
            $scope.newName = '';
            $scope.newEmail = '';
            $scope.searchText = '';
            $scope.currentPage = 0;
            $scope.Header = ['', '', ''];
        };

        $scope.add = function () {
            $scope.allItems.push({
                EmpId: $scope.newEmpId,
                name: $scope.newName,
                Email: $scope.newEmail
            });
            $scope.resetAll();
        };

        $scope.search = function () {
            $scope.filteredList = filteredListService.searched($scope.allItems, $scope.searchText);

            if ($scope.searchText === '') {
                $scope.filteredList = $scope.allItems;
            }
            $scope.pagination();
        };

        $scope.pagination = function () {
            $scope.ItemsByPage = filteredListService.paged($scope.filteredList, $scope.pageSize);
        };

        $scope.setPage = function () {
            $scope.currentPage = this.n;
        };

        $scope.firstPage = function () {
            $scope.currentPage = 0;
        };

        $scope.lastPage = function () {
            $scope.currentPage = $scope.ItemsByPage.length - 1;
        };

        $scope.range = function (input, total) {
            var ret = [];
            if (!total) {
                total = input;
                input = 0;
            }
            for (var i = input; i < total; i++) {
                if (i !== 0 && i !== total - 1) {
                    ret.push(i);
                }
            }
            return ret;
        };

        $scope.sort = function (sortBy) {
            $scope.resetAll();
            $scope.columnToOrder = sortBy;
            $scope.filteredList = $filter('orderBy')($scope.filteredList, $scope.columnToOrder, $scope.reverse);
            if ($scope.reverse) iconName = 'glyphicon glyphicon-chevron-up';
            else iconName = 'glyphicon glyphicon-chevron-down';
            if (sortBy === 'EmpId') {
                $scope.Header[0] = iconName;
            } else if (sortBy === 'name') {
                $scope.Header[1] = iconName;
            } else {
                $scope.Header[2] = iconName;
            }
            $scope.reverse = !$scope.reverse;
            $scope.pagination();
        };

        $scope.sort('name');
    }, function errorCallback(response) {
        console.log(response)
    });

});
app.service('filteredListService', function () {
    this.searched = function (valLists, toSearch) {
        return _.filter(valLists,
            function (i) {
                return searchUtil(i, toSearch);
            });
    };

    this.paged = function (valLists, pageSize) {
        retVal = [];
        for (var i = 0; i < valLists.length; i++) {
            if (i % pageSize === 0) {
                retVal[Math.floor(i / pageSize)] = [valLists[i]];
            } else {
                retVal[Math.floor(i / pageSize)].push(valLists[i]);
            }
        }
        return retVal;
    };

});

function searchUtil(item, toSearch) {
    return (item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.Email.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.EmpId === toSearch);
}

app.controller("calendarCtrl", function ($scope, $http, $rootScope) {
    $(document).ready(function () {
        $('#calendar').fullCalendar({
            height: 550,
            eventColor: '#378006',
            navLinks: true,
            events: function (start, end, timezone, callback) {
                var request = $http({
                    method: 'get',
                    url: 'http://localhost:3334/calendarEvents'
                }).then(function successCallback(response, data) {
                    var events = [];
                    response.data.forEach(function (element) {
                        console.log(element.startDate);
                        events.push({
                            title: element.title,
                            start: new Date(element.startDate),
                            end: new Date(element.endDate),
                            backgroundColor: '#ff0006'
                        });
                    });
                    callback(events);
                }, function errorCallback(response) {
                    alert(response);
                });
            },
            selectable: true,
            selectHelper: true,
            select: function (start, end) {
                $('.modal').modal('show');
            },
            eventClick: function (event, element) {
                $('.modal').modal('show');
                $('.modal').find('#title').val(event.title);
                $('.modal').find('#starts-at').val(event.start);
                $('.modal').find('#ends-at').val(event.end);
            },
            editable: true,
            eventLimit: true
        });
        $("#starts-at, #ends-at").datetimepicker();

        $('#save-event').on('click', function () {
            var title = $('#title').val();
            if (title) {
                var eventData = {
                    title: title,
                    start: $('#starts-at').val(),
                    end: $('#ends-at').val()
                };
                $('#calendar').fullCalendar('renderEvent', eventData, true);
            }
            $('#calendar').fullCalendar('unselect');
            $('.modal').find('input').val('');
            $('.modal').modal('hide');
        });
    });
});

app.controller("dosarCtrl", [dosarCtrl]);

function dosarCtrl() {
    var vm = this;

    vm.currentStep = 1;
    vm.steps = [
        {
            step: 1,
            name: "Detalii dosar",
            template: "templates/dosarDetailsSteps/stepsDetails/detaliidosar.html"
        }, {
            step: 2,
            name: "Participanti",
            template: "templates/dosarDetailsSteps/stepsDetails/Participanti.html"
        }, {
            step: 3,
            name: "Documentea Atasate",
            template: "templates/dosarDetailsSteps/stepsDetails/documenteAtasate.html"
        }, {
            step: 4,
            name: "Plati",
            template: "templates/dosarDetailsSteps/stepsDetails/Plati.html"
        }, {
            step: 5,
            name: "Documente Create",
            template: "templates/dosarDetailsSteps/stepsDetails/createdDocs.html"
        }, {
            step: 6,
            name: "Istoric Actiunii",
            template: "templates/dosarDetailsSteps/stepsDetails/istoricActiunii.html"
        }
    ];
    vm.user = {};

    vm.gotoStep = function (newStep) {
        vm.currentStep = newStep;
    };

    vm.getStepTemplate = function () {
        for (var i = 0; i < vm.steps.length; i++) {
            if (vm.currentStep === vm.steps[i].step) {
                return vm.steps[i].template;
            }
        }
    };

    vm.save = function () {
        alert(
            "Saving form... \n\n" +
            "Name: " + vm.user.inst + "\n" +
            "Email: " + vm.user.document + "\n" +
            "Age: " + vm.user.age + "\n" +
            "Date" + vm.user.date);
    }
}


app.controller("buttonCtrlinAdminPanel", [buttonCtrlinAdminPanel]);

function buttonCtrlinAdminPanel() {
    var vm = this;

    vm.currentStep = 1;
    vm.steps = [
        {
            step: 1,
            name: "Organization",
            template: "admin/tableButtons/organization.html"
        },
        {
            step: 2,
            name: "Roles",
            template: "admin/tableButtons/roles.html"
        },
        {
            step: 3,
            name: "Premissions",
            template: "templates/steps/step3.html"
        }
    ];
    vm.user = {};

    vm.gotoStep = function (newStep) {
        vm.currentStep = newStep;
    };

    vm.getStepTemplate = function () {
        for (var i = 0; i < vm.steps.length; i++) {
            if (vm.currentStep === vm.steps[i].step) {
                return vm.steps[i].template;
            }
        }
    };

    vm.save = function () {
        alert(
            "Saving form... \n\n" +
            "Name: " + vm.user.inst + "\n" +
            "Email: " + vm.user.document + "\n" +
            "Age: " + vm.user.age + "\n" +
            "Date" + vm.user.date);
    }
}
app.controller("GenerateDocumentController" , function ($scope , $state , $http , $rootScope) {

});